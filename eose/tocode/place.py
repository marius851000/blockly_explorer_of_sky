from eose.tocode.script import script

class place:
    """ceci représente un lieu"""
    def __init__(self, name="T00P01"):
        self.scripts = {} # scripts ( "nom" : script(object) )
        self.name = name

    def addScript(self, sc):
        """ajoute un script
        input :
        sc : script - le script à ajouter au lieux"""
        assert type(sc) == script
        self.scripts["enter"] = sc

    def toXML(self):
        """transforme en xml
        output :
        str - xml ( ppmdu-tools like )"""
        rendu = ""
        rendu = rendu + "<?xml version=\"1.0\"?>"
        rendu = rendu + "<Level gameVersion=\"EoS\" gameRegion=\"Europe\">"

        for loop in self.scripts:
            rendu = rendu + self.scripts[loop].toXML()

        rendu = rendu + "</Level>"

        return rendu

if __name__ == '__main__':
    a = open("../save.xml")
    b = a.read()
    a.close()
    a = script(b)
    p = place()
    p.scripts["enter"] = a
    print(p.toXML())
