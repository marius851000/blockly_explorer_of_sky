class manager:
    """gére les différent type de donné"""
    def __init__(self):
        self.places = {} # tous les lieux
        self.saveDirectory = None # dossier de sauvegarde

    def addPlace(self, place):
        """ajout d'un lieux dans ce manager"""
        from eose.tocode.place import place as PLACE
        assert type(place) == PLACE
        self.places[place.name] = place

    def setSaveDirectory(self, directory):
        """définie un répértoire de sauvegarde
        input :
        * directory : str - où sauvegarder"""
        assert type(directory) == str
        self.saveDirectory = directory

    def saveInit(self):
        """initialise le dossier de sauvegarde"""
        self.needSaveDirectory()
        from eose.save.init import init
        init(self.saveDirectory)

    def saveSave(self):
        """sauvegarde le manager ( le dossier de sauvegarde doit être définie )"""
        self.needSaveDirectory()
        from eose.save.save import save
        for place in self.places:
            save(self.saveDirectory, typee="place", data=self.places[place])

    def saveLoad(self):
        """charge à partir du fichier de sauvegarde"""
        self.needSaveDirectory()
        from eose.save.load import load
        retour = load(self.saveDirectory)
        try: # TODO: cleaner code
            self.places = retour["place"]
        except:
            self.places = {}

    def needSaveDirectory(self):
        """vérifie si le dossier de sauvegarde est définie ( sinon leve un erreur )
        output :
        bool - est ce définie ?"""
        if type(self.saveDirectory) == str:
            return True
        raise

    def display(self):
        """affiche les info dans un format human readable"""
        print("lieux :")
        for place in self.places:
            print(place + ":")
            print(self.places[place].toXML())
