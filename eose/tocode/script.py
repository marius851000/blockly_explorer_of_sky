from eose.tocode.blocklyToCode import blocklyToCode
from eose.tocode.codeToXml import codeToXml

class script:
    """cette classe représente un script."""
    def __init__(self,code = None, codetype = "blockly"):
        """code doit être sous la forme d'un code ( xml sous forme de string ) sorti par blockly ( voir index.html )"""
        self.code = []
        if code != None:
            if codetype == "blockly":
                self.fromBlockly(code)
            else:
                raise Exception("codetype doit être : \"blockly\".")

        self.name = "enter"

    def fromBlockly(self, blocklyCode, name="enter00"):
        self.addScript(name = name, fonctions = [{"type" : "Function", "id" : 0, "code" : blocklyToCode(blocklyCode)}])

    def addScript(self, name, fonctions):
        self.code.append({
            "name" : "enter00",
            "composed" : fonctions
        })

    def toXML(self):
        rendu = ""

        #scriptset
        rendu = rendu + "<ScriptSet name=\"" + self.name + "\">"

        rendu = rendu + codeToXml(self.code)

        rendu = rendu + "\n</ScriptSet>"
        return rendu
