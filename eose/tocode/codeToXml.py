_lang = ["English","French","German","Italian","Spanish"]

def codeToXml(code):
    """retourne code sous forme d'XML'"""
    rendu = ""

    for scriptSequence in code: # pour chaque scriptSequence
        rendu = rendu + "\n<ScriptSequence name=\"" + scriptSequence["name"] + "\">"
        rendu = rendu + "\n<Code>"

        for function in scriptSequence["composed"]: # pour chaque fonction
            renduFunction = ""
            for part in function["code"]:
                renduFunction = renduFunction + "\n" + commandToXml(part)
            rendu = rendu + "\n<"+function["type"] + " _id=\"" + str(function["id"]) + "\">"
            rendu = rendu + renduFunction
            rendu = rendu + "\n</"+function["type"] + ">"

        rendu = rendu + "\n</Code>"
        rendu = rendu + "\n</ScriptSequence>"

    return rendu


def commandToXml(command,translation={}):
    tag = command["type"]
    
    if tag == "script_start_function":
        return ""
    elif tag == "sound_stop":
        return "<sound_Stop />"
    elif tag == "message_mail":
        # TODO: translation
        rendu = "<message_Mail>"
        for loop in _lang:
            rendu = rendu + "\n\t<String language=\""+loop+"\">"+command["param"]["TEXT"]+"</String>"
        rendu = rendu + "</message_Mail>"
        return rendu

    return "<!-- erreur -->"
