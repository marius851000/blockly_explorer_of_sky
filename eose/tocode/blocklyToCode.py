from lxml import etree

class block:
    """un block de blockly"""
    def __init__(self, xml):
        pass

def blocklyToCode(code):
    code = "<xml>"+code[42:len(code)]
    root = etree.fromstring(code)
    blocks = root.findall("block")
    for block in blocks:
        rendu  = toCode(block)

    return rendu

def toCode(code):
    a = []
    attrib = code.attrib
    param = code.findall("value")
    parame = {}
    for value in param:
        block = value.find("block")
        field = block.find("field")
        parame[value.attrib["name"]] = field.text

    a.append({
        "type" : attrib["type"],
        "param" : parame
        })
    nexte = code.findall("next")

    toAdd = None
    for loop in nexte:
        block = loop.find("block")
        toAdd = toCode(block)

    if toAdd != None:
        for loop in toAdd:
            a.append(loop)
    return a
