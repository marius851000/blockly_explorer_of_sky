import os
from lxml import etree

def save(localisation,typee,data):
    """save the data data, of type typee, a the location localisation"""
    fichier = open(localisation+"/modification.xml")
    texte = fichier.read()
    fichier.close()

    root = etree.fromstring(texte)

    #adding the modification to modification.xml
    modification = etree.SubElement(root, "modification")
    modification.attrib["type"] = typee

    retour = {"data" : "None"}

    if typee == "place":
        from eose.save.savePlace import savePlace
        retour = savePlace(localisation=localisation, data=data)
    else:
        raise Exception("The input typee of save insnut a know type.")

    modification.attrib["data"] = retour["data"]

    #writing modification
    texteB = etree.tostring(root)
    fichier = open(localisation+"/modification.xml","wb")
    fichier.write(texteB)
    fichier.close()

if __name__ == '__main__':
    init("here")
    save(localisation="here",typee="place",data="nop")
