from lxml import etree
from eose.save.loadPlace import loadPlace

def load(localisation):
    # liste des typee depuis modification.xml
    fichier = open(localisation+"/modification.xml")
    texte = fichier.read()
    fichier.close()

    root = etree.fromstring(texte)

    rendu = {}
    for modification in root.findall("modification"):
        modTypee = modification.attrib["type"]
        uid = modification.attrib["data"]
        retour = {}
        if modTypee == "place":
            retour = loadPlace(localisation, uid)
        else:
            raise Exception("sauvegarde incohérente : modification de type inconnu ( /modification.xml )")

        if not modTypee in rendu.keys():
            if modTypee == "place":
                rendu[modTypee] = {}

        if modTypee == "place":
            rendu[modTypee][retour.name] = retour

    return rendu
