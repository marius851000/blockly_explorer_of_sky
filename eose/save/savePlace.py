import eose.save.util as util
import os.path
from lxml import etree

def savePlace(localisation, data):
    name = data.name
    util.makeDirIfNotExist(localisation+"/place")
    util.makeDirIfNotExist(localisation+"/place/"+name)

    directory = localisation + "/place/" + name

    if not os.path.isfile(directory+"/modification.xml"): #create modification.xml if it dont exist
        fichier = open(directory+"/modification.xml","w")
        fichier.write("""<data name=\""""+name+"""\" >
<add>
</add>
</data>""")
        fichier.close()

    for loop in data.scripts:
        here = data.scripts[loop]
        addScript(directory, here.name, here)

    return {"data":name}

def addScript(directory, name, data):
    """créer les fichier de base pour un lieu name"""
    fichier = open(directory+"/"+name+".xml", "w")
    fichier.write(data.toXML())
    fichier.close()

    #modification.xml
    fichier = open(directory+"/modification.xml")
    texte = fichier.read()
    fichier.close()
    root = etree.fromstring(texte)
    add = root.find("add")

    #check if it isn't already in the modification ( based on name )
    scriptAlreadySaved = False
    for loop in add.findall("script"):
        if loop.attrib["name"] == name:
            scriptAlreadySaved = True

    #edit modification.xml
    if not scriptAlreadySaved:
        script = etree.SubElement(add, "script")
        script.attrib["name"] = name

    texteB = etree.tostring(root)
    fichier = open(directory+"/modification.xml","wb")
    fichier.write(texteB)
    fichier.close()
