from lxml import etree
from eose.tocode.place import place
from eose.fromGame.toScript import toScript

def loadPlace(localisation, uid):
    directory = localisation+"/place/"+uid
    # loading the modification file of the place
    fichier = open(directory+"/modification.xml")
    texte = fichier.read()
    fichier.close()

    root = etree.fromstring(texte)
    additions = root.find("add")

    renduPlace = place()
    renduPlace.name = root.attrib["name"]

    for addition in additions:
        tag = addition.tag
        if tag == "script":
            renduPlace.scripts[addition.attrib["name"]] = loadScript(directory, addition.attrib["name"])
        else:
            raise Exception("fichier incohérent : type inconnu dans la balise add de modification de " + uid + ".")

    return renduPlace

def loadScript(directory, uid):
    fichier = open(directory + "/" + uid + ".xml")
    texte = fichier.read()
    fichier.close()
    script = toScript(texte)
    return script
