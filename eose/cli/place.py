def do(param, game, p):
    if param.action2 == "load": # load script
        if param.action3 == "blockly": #load blockly script
            p.echo("  [..]loading script file",2)
            a = open(param.path)
            blocklyCode = a.read()
            print(blocklyCode)
            a.close()
            p.echo("  [OK]loaded",2)
            p.echo("  [..]importing script",2)
            from eose.tocode.script import script
            p.echo("  [OK]imported",2)
            p.echo("  [..]importing the script",1)
            s = script(blocklyCode)
            p.echo("  [OK]done !",1)
            p.echo("  [..]adding to game")
            game.places[param.place].addScript(s)
            p.echo("  [OK]added")

    elif param.action2 == "create":
        from eose.tocode.place import place
        p = place(name=param.place)
        game.addPlace(p)
