class console:
    def __init__(self,logLevel=2): # 0 = nothing, 1 = normal, 2 = all
        self.logLevel = logLevel

    def echo(self, text, priority=1):
        if priority<=self.logLevel:
            print(text)
