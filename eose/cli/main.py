def cli(param):
    """cli:
    saveDirectory : dossier de sauvegarde ( str )
    action : les différente actions possible ( str ):
        "init" :
            initialise la sauvegarde
        "display" :
            affiche la sauvegarde dans un format human readable
        "createPlace" :
            créer un lieux
                place : nom du lieux
        "place":
            tous ce qui est lieu
            "create":
                créer un lieux
                    place : nom du lieux
            "load":
                charge un script
                "blockly":
                    charge un script blockly like:
                        place : nom du lieux ou il faut l'ajouter
                        path : chemin du fichier ( normalement save.xml )
    """
    from eose.cli.console import console as c
    p = c(1)
    p.echo("welcome to marius's PMD:EOS modding tools",1)# TODO: better name
    p.echo("[..]checking variable",2)

    assert type(param.saveDirectory) == str

    p.echo("[OK]variable checked",2)
    p.echo("[..]creating manager",2)

    from eose.tocode.manager import manager
    game = manager()
    p.echo("  [..]setting save dir",2)
    game.setSaveDirectory(param.saveDirectory)
    p.echo("  [OK]save directory setted",2)

    p.echo("[OK]manger created",2)

    action = param.action
    if action != "init":# TODO: load save
        p.echo("[..]Loading save",1)
        game.saveLoad()
        p.echo("[OK]Loaded",1)


    p.echo("[..]doing : \'"+action+"\'...",1)
    p.echo("  [..]importing '"+action+"'",1)
    import eose.cli.all as all
    comm = getattr(all,action)
    p.echo("  [OK]imported",1)

    comm.do(param, game, p)
    p.echo("[OK]action finished",1)

    p.echo("[..]saving",1)
    game.saveSave()
    p.echo("[OK]saved")
