def do(param, game, p):
    """initialise la sauvegarde ( la remet à 0 !!! )"""
    saveDir = param.saveDirectory
    p.echo("  [..]Initialising save directory in "+saveDir+".",1)
    a = input("êtes vous sur de vouloir réinitialiser la sauvegarde ? (O/n)")
    if a == "O" or a == "o":
        game.saveInit()
        p.echo("  [OK]Initialised",1)
        return

    p.echo("  [OK]Cancelled",1)
