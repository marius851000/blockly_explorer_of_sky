from eose.tocode.script import script
from lxml import etree

def toScript(texte):
    """load a script ( ScriptSet and all that go with this )"""
    ScriptSet = etree.fromstring(texte)
    ScriptSequences = ScriptSet.findall("ScriptSequence")
    rendu = []

    for scriptSequence in ScriptSequences:# each script sequence
        scriptSequenceRendu = {
            "name" : scriptSequence.attrib["name"],
            "composed" : []
        }

        code = scriptSequence.find("Code")
        for function in code:#each function
            functionRendu = {
                "type" : function.tag,
                "id" : int(function.attrib["_id"]),
                "code" : []
            }
            retour = XMLToInstruction(function)

            functionRendu["code"] = retour

            scriptSequenceRendu["composed"].append(functionRendu)

        rendu.append(scriptSequenceRendu)

    Script = script()
    Script.code = rendu
    Script.name = "enter" # todo
    return Script

def XMLToInstruction(xmltree):
    rendu = []
    for instruction in xmltree:
        tag = instruction.tag
        retour = { "type" : tag,
            "param" : {}}
        if tag == "message_Mail":
            retour["type"] = "message_mail"
            retour["param"]["TEXT"] = instruction.find("String").text
        elif tag == "sound_Stop":
            retour["type"] = "sound_stop"
        else: # default treatement
            # TODO: prendre en compte les argument pour les donné d'instruction vide
            pass

        rendu.append(retour)
    return rendu
