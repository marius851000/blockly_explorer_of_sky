def executeCMD():
    import argparse

    parser = argparse.ArgumentParser(prog="eoss",
                                    description=("a tool for mystery dungeon : explorer of sky modding"),)
    # action
    parser.add_argument("action", nargs='?', type=str, help=("first action"))
    parser.add_argument("action2", nargs='?', type=str, help=("second action"))
    parser.add_argument("action3", nargs='?', type=str, help=("third action"))

    # save directory
    parser.add_argument('-s',"--saveDirectory", type=str, help=("folder containing the actual mod worked on"))

    # place
    parser.add_argument('-p','--place', type=str, help=("the name of the place"))

    # path
    parser.add_argument("--path", type=str, help=("the path of the script to load"))


    args = parser.parse_args()

    from eose.cli.main import cli
    cli(args)

if __name__ == '__main__':
    executeCMD()
