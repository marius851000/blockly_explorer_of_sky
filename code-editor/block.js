Blockly.Blocks['script_start_function'] = {
  init: function() {
    this.appendDummyInput()
        .appendField("Start function");
    this.setNextStatement(true, null);
    this.setColour(135);
 this.setTooltip("When the script's function start.");
 this.setHelpUrl("");
  }
};

Blockly.Blocks['sound_stop'] = {
  init: function() {
    this.appendDummyInput()
        .appendField("Stop all sound");
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setColour(230);
 this.setTooltip("stop all sound.");
 this.setHelpUrl("");
  }
};

Blockly.Blocks['message_mail'] = {
  init: function() {
    this.appendValueInput("TEXT")
        .setCheck("String")
        .appendField("mail message");
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setColour(230);
 this.setTooltip("");
 this.setHelpUrl("");
  }
};
