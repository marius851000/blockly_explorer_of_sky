# outil de modification de pokémon donjon mystère : explorateur du ciel #
Ce répertoire contient une collection d'outil permettant de modifier le jeu pokémon donjon mystère : explorateur du ciel.

Actuellement ce répertoire ne permet que de modifier les séquences scriptés ( script ), mais je ( marius851000 ) compte ajouter un système unifié permettant de gérer une modification d'explorateur du ciel.

pour quelques exemples, voir le fichier example.md

TODO :
- ajouter un système permettant d'organiser une modification ( voir fr/todo/sauvegarde )
