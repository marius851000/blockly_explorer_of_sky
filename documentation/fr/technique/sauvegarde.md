# systéme de sauvegarde de cet outil #
Mon ( marius851000 ) idée pour gérer les sauvegarde est de tout mettre dans un dossier.

ce dossier contient un fichier nommé modification.xml, qui contient la liste des modification à apporter

exemple d'un répertoire de base :

/
|-- modification.xml

exemple de fichier modification.xml :

    <modifications>
      <modification type="place" data="TOOPO1" />
    </modifications>

une modification par ligne, avec le type et id pour son pour retrouver le fichier correspondant ( souvent /type/id/modification.xml ).
