# modification des scripts #

/
|-- modification.xml
|-- place
  |-- T00P01
    |-- modification.xml
    |-- enter.xml

Le fichier enter.xml est juste un fichier qui contient un ScriptSet tel qu'exporté par ppmdu-tools
exemple :

<ScriptSet name="enter">
  <ScriptSequence name="enter00">
    <Code>
      <Function _id="0">
        <message_Mail>
	  <String language="English">Hello World !</String>
	  <String language="French">Hello World !</String>
	  <String language="German">Hello World !</String>
	  <String language="Italian">Hello World !</String>
	  <String language="Spanish">Hello World !</String>
        </message_Mail>
        <sound_Stop />
        <message_Mail>
	  <String language="English">Tadada ! No sound now !</String>
	  <String language="French">Tadada ! No sound now !</String>
	  <String language="German">Tadada ! No sound now !</String>
	  <String language="Italian">Tadada ! No sound now !</String>
	  <String language="Spanish">Tadada ! No sound now !</String>
        </message_Mail>
      </Function>
    </Code>
  </ScriptSequence>
</ScriptSet>
