# quelque exemple de cette outil, pour référence #
## manager
### importer le manager et l'initialiser :
  from tocode.manager import manager
  game = manager()

### définir, initialiser et sauvegarder :
  from tocode.manager import manager
  game = manager()
  game.setSaveDirectory("here") # the directory where we save
  game.saveInit()
  game.saveSave() # save in the game directory referenced before

### charge une sauvegarde :
  from tocode.manager import manager
  game = manager()
  game.setSaveDirectory("here")
  game.saveLoad()
  print(game.places) # for test only

## place
un lieux est représenté par la variable place
### définir un lieux
  from tocode.manager import manager
  game = manager()

  from tocode.place import place
  lieu = place(name="T00P01") # map de test

  # ajoute le lieux
  game.addPlace(lieu)
  print(game.places) # pour le test

## script
  from tocode.manager import manager
  game = manager()

  from tocode.place import place
  lieu = place(name="T00P01") # map de test

  # ajoute le place
  game.addPlace(lieu)

  #charge le script
  scriptB = open("code-editor/save.xml").read() # lecture
  from tocode.script import script
  scriptA = script(scriptB)

  lieu.addScript(scriptA)

  print(lieu.toXML())
